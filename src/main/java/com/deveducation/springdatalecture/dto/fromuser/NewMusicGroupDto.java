package com.deveducation.springdatalecture.dto.fromuser;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import javax.validation.constraints.*;

@Value
public class NewMusicGroupDto {
    @Size(min = 2, max = 255, message = "length of album name must be in a range from 2 to 255")
    @NotNull(message = "must not be null")
    String name;

    @Min(value = 1860, message = "must be no less than 1860")
    short year_creation;


    short year_decay;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public NewMusicGroupDto(@JsonProperty("name") String name,
                            @JsonProperty("year_creation") short yearCreation,
                            @JsonProperty("year_decay") short yearDecay) {
        this.name = name;
        this.year_creation = yearCreation;
        this.year_decay = yearDecay;
        ;
    }
}
