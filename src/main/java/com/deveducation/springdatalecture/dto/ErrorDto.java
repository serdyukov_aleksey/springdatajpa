package com.deveducation.springdatalecture.dto;

import lombok.Value;

@Value
public class ErrorDto {

    String field;
    String value;
    String message;
}
