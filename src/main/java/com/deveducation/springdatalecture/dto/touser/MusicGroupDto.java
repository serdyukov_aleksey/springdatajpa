package com.deveducation.springdatalecture.dto.touser;

import lombok.Value;

import java.util.List;

@Value
public class MusicGroupDto {
    String name;
    int creationYear;
    int decayYear;
    int availableAlbums;
    List<String> genres;
}