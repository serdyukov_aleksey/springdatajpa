package com.deveducation.springdatalecture.service.musicgroup;

import com.deveducation.springdatalecture.constants.ErrorConstants;
import com.deveducation.springdatalecture.dto.ErrorDto;
import com.deveducation.springdatalecture.dto.fromuser.EditMusicGroupDto;
import com.deveducation.springdatalecture.dto.fromuser.NewMusicGroupDto;
import com.deveducation.springdatalecture.entity.MusicGroup;
import com.deveducation.springdatalecture.exception.UnprocessableEntityException;
import com.deveducation.springdatalecture.springdata.IAlbumSpringDataRepo;
import com.deveducation.springdatalecture.springdata.IGenreSpringDataRepo;
import com.deveducation.springdatalecture.springdata.IMusicGroupSpringDataRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("springDataGroupService")
@ConditionalOnProperty(name = "db.connector", havingValue = "springData")
public class SpringDataGroupService implements IMusicGroupService {

    private final IMusicGroupSpringDataRepo musicGroupRepo;
    private final IGenreSpringDataRepo genreRepo;
    private final IAlbumSpringDataRepo albumRepo;

    @Autowired
    public SpringDataGroupService(IMusicGroupSpringDataRepo musicGroupRepo,
                                  IGenreSpringDataRepo genreRepo,
                                  IAlbumSpringDataRepo albumRepo) {
        this.musicGroupRepo = musicGroupRepo;
        this.genreRepo = genreRepo;
        this.albumRepo = albumRepo;
    }

    @Override
    public void setMusicGroup(NewMusicGroupDto musicGroupDto) {
        MusicGroup musicGroup= new MusicGroup();
        musicGroup.setName(musicGroupDto.getName());
        musicGroup.setYearCreation(musicGroupDto.getYear_creation());
        musicGroup.setYearDecay(musicGroupDto.getYear_decay());
        musicGroupRepo.save(musicGroup);
    }

    private MusicGroup getMusicGroup(String name) {
        return musicGroupRepo.findByNameIgnoreCase(name).orElseThrow(() ->
                new UnprocessableEntityException(new ErrorDto(ErrorConstants.Fields.MUSIC_GROUP, name, ErrorConstants.Msgs.NOT_FOUND)));
    }

    @Override
    @Transactional
    public void editMusicGroup(EditMusicGroupDto editMusicGroupDto) {
        MusicGroup oldMusicGroup = musicGroupRepo.findByNameIgnoreCase(editMusicGroupDto.getOldName()).orElseThrow(() ->
                new UnprocessableEntityException(new ErrorDto(ErrorConstants.Fields.MUSIC_GROUP, editMusicGroupDto.getOldName(), ErrorConstants.Msgs.NOT_FOUND)));
        MusicGroup editedMusicGroup = new MusicGroup();
        editedMusicGroup.setName(editMusicGroupDto.getName());
        editedMusicGroup.setYearCreation(editMusicGroupDto.getYear_creation());
        editedMusicGroup.setYearDecay(editMusicGroupDto.getYear_decay());
        if((editedMusicGroup.getName() !=null && editedMusicGroup.getYearCreation() !=0) &&
        musicGroupRepo.existsByName(editedMusicGroup.getName())){
            oldMusicGroup.setName(editedMusicGroup.getName());
            oldMusicGroup.setYearCreation(editedMusicGroup.getYearCreation());
            oldMusicGroup.setYearDecay(editedMusicGroup.getYearDecay());
        }

    }

}
