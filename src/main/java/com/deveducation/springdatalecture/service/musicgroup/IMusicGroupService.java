package com.deveducation.springdatalecture.service.musicgroup;


import com.deveducation.springdatalecture.dto.fromuser.EditMusicGroupDto;
import com.deveducation.springdatalecture.dto.fromuser.NewMusicGroupDto;

public interface IMusicGroupService {

    void setMusicGroup(NewMusicGroupDto musicGroup);
    void editMusicGroup(EditMusicGroupDto editMusicGroupDto);
//    void setMusicGroups(List<EditMusicGroupDto> musicGroupList);
}
