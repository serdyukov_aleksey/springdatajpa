package com.deveducation.springdatalecture.service.musicalbum;


import com.deveducation.springdatalecture.dto.MusicAlbumDto;
import com.deveducation.springdatalecture.dto.fromuser.EditMusicAlbumDto;

import java.util.Set;

public interface IMusicAlbumService {
    void addMusicAlbum(MusicAlbumDto musicAlbum);
    void editMusicAlbum(EditMusicAlbumDto musicAlbum);
    Set<MusicAlbumDto> getMusicAlbum(Set<String> names);
    Set<MusicAlbumDto> getMusicAlbumByGenre(String genre);
    Set<MusicAlbumDto> getMusicAlbumByYearRelease(short from, short to);
}
