package com.deveducation.springdatalecture.exception;

import com.deveducation.springdatalecture.dto.ErrorDto;
import lombok.Getter;

import java.util.Collections;
import java.util.List;

public class UnprocessableEntityException extends RuntimeException {

    @Getter
    private final List<ErrorDto> errors;

    public UnprocessableEntityException(ErrorDto error) {
        this.errors = Collections.singletonList(error);
    }

}
