package com.deveducation.springdatalecture.springdata;

import com.deveducation.springdatalecture.entity.Genre;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@ConditionalOnProperty(name = "db.connector", havingValue = "springData")
public interface IGenreSpringDataRepo extends CrudRepository<Genre, Long> {
    Optional<Genre> findByNameIgnoreCase(String name);

}
