package com.deveducation.springdatalecture.springdata;

import com.deveducation.springdatalecture.entity.MusicGroup;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "MusicGroup", path = "musicgroup")
public interface IMusicGroupSpringDataRestRepo extends PagingAndSortingRepository<MusicGroup, Long> {

    List<MusicGroup> findByNameIgnoreCase(@Param("name") String name);
}
