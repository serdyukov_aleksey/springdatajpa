package com.deveducation.springdatalecture.springdata;

import com.deveducation.springdatalecture.entity.Album;
import com.deveducation.springdatalecture.entity.Genre;
import com.deveducation.springdatalecture.entity.MusicGroup;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
@ConditionalOnProperty(name = "db.connector", havingValue = "springData")
public interface IAlbumSpringDataRepo extends CrudRepository<Album, Long> {
    boolean existsByNameIgnoreCaseAndMusicGroup(String name, MusicGroup musicGroup);
    Optional<Album> findByNameIgnoreCaseAndMusicGroup(String name, MusicGroup musicGroup);
    List<Album> findByNameInIgnoreCase(Set<String> name);
    List<Album> findAlbumsByGenre (Genre genre);
    List<Album> findAlbumsByYearReleaseBetween (short from, short to);
}
