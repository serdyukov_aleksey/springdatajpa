package com.deveducation.springdatalecture.springdata;

import com.deveducation.springdatalecture.entity.MusicGroup;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.Optional;

@Repository
@ConditionalOnProperty(name = "db.connector", havingValue = "springData")
public interface IMusicGroupSpringDataRepo extends CrudRepository<MusicGroup, Long> {
    Optional<MusicGroup> findByNameIgnoreCase(String name);
    MusicGroup save (MusicGroup musicGroup);
    List<MusicGroup> save (List<MusicGroup> musicGroupList);
    boolean existsByName (String name);
}
