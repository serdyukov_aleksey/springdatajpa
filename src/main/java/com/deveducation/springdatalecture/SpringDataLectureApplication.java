package com.deveducation.springdatalecture;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDataLectureApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringDataLectureApplication.class, args);
    }

}
