package com.deveducation.springdatalecture.springdata;

import com.deveducation.springdatalecture.entity.Album;
import com.deveducation.springdatalecture.entity.Genre;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class IAlbumSpringDataRepoTest {

    @Autowired
    private  IAlbumSpringDataRepo cut;

    @Test
    public void findAlbumsByGenre() {
        Genre genre = new Genre();
        genre.setId(3l);
        genre.setName("deathcore");
        List<Album> albums = cut.findAlbumsByGenre(genre);
        assertEquals(albums.size(), 2);
    }

    @Test
    public void findAlbumsByYearReleaseBetween() {
        List<Album> albums = cut.findAlbumsByYearReleaseBetween((short)2004, (short)2010);
        assertEquals(albums.size(), 16);
    }
}