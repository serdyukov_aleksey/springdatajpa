package com.deveducation.springdatalecture.springdata;

import com.deveducation.springdatalecture.entity.Genre;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class IGenreSpringDataRepoTest {

    @Autowired
    private IGenreSpringDataRepo cut;

    @Test
    public void findByNameIgnoreCase() {
        Optional<Genre> genre = cut.findByNameIgnoreCase("progressive metal");
        assertEquals(genre.get().getId(), 5);
        assertEquals(genre.get().getName(), "progressive metal");
    }
}
